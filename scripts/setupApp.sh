#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

# Variables

# Set the system timezone to UTC
echo "Setting system timezone to UTC"
mv /etc/localtime /etc/localtime.bak
ln -s /usr/share/zoneinfo/UTC /etc/localtime

# Install global system dependencies
if  [[ -x "$(command -v yum)" ]]; then    # centos system
    echo "Found CENTOS machine, installing system dependencies"

    # Install ghost script for garbled text issue for google chart
    # (https://confluence.atlassian.com/confkb/confluence-ui-shows-garbled-or-corrupt-text-on-captcha-macros-and-or-diagrams-due-to-missing-fonts-938027858.html)
    yum install ghostscript wget make gcc* cairo cairo-devel pango-devel \
    libjpeg-devel libjpeg-turbo-devel giflib-devel jq vim -y
    yum clean all
    yum install nodejs
fi

if  [[ -x "$(command -v apt-get)" ]]; then    # ubuntu system
    echo "Found UBUNTU machine, installing system dependencies"

    ${PROJECT_DIR}/scripts/utils/wait-for-apt.sh

    # Install ghost script for garbled text issue for google chart
    # (https://confluence.atlassian.com/confkb/confluence-ui-shows-garbled-or-corrupt-text-on-captcha-macros-and-or-diagrams-due-to-missing-fonts-938027858.html)
    apt-get install ghostscript wget make gcc* build-essential libcairo2-dev \
    libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev jq vim -y
    apt-get clean all
fi

cd /opt/demo-apps
npm init
npm install express --save

node index.js

echo "Project setup is done successfully."
